#ifndef GESTIONNAIREOVO_HPP
#define GESTIONNAIREOVO_HPP

#include "GestionnaireScene.hpp"
#include "ScenesOvo.hpp"

/**
 * @class GestionnaireOvo
 * @author cedric
 * @date 09/05/15
 * @file GestionnaireOvo.hpp
 * 
 * Représente un gestionnaire de scène pour un pong
 */
class GestionnaireOvo : public GestionnaireScene {
public:
  GestionnaireOvo() : GestionnaireScene (new SceneJeuOvo()) { }

public:
  bool changementDeScene();
};

#endif // GESTIONNAIREOVO_HPP
