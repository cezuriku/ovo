#ifndef GESTIONNAIRESCENE_HPP
#define GESTIONNAIRESCENE_HPP

#include <memory>
#include "Scene.hpp"

/**
 * @class GestionnaireScene
 * @author cedric
 * @date 09/05/15
 * @file GestionnaireScene.hpp
 * Gestionnaire possèdant plusieurs scènes
 *
 * Classe abstraite représentant un gestionnaire de scène.
 * Un gestionnaire de scène doit pouvoir retourner une scène tant qu'il n'est
 * pas terminé.
 * La fonction changementDeScene sera appelé entre chaque actualisation et c'est
 * dans cette fonction uniquement que le gestionnaire peut changer de scène.
 *
 * @see Scene
 */
class GestionnaireScene {
public:
  /**
   * Accesseur scene
   *
   * Récupère la scène courante si elle est active sinon retourne un shared_ptr
   * null
   *
   * @return scene
   */
  inline std::shared_ptr<Scene> getScene() {
	  return scene;
  }

  /**
   * Demande si c'est le moment du changement de scène
   *
   * Cette méthode doit être utilisé pour informer que c'est le moment si l'on
   * veut changer de scène et récupère un booléen pour savoir si le changement
   * de scène à été effectué.
   * 
   * @return vrai si il y a eu un changement de scène
   */
  virtual bool changementDeScene() = 0;

protected:
  /**
   * Constructeur par défaut
   * @param _scene La première scène à jouer
   * @param _numeroScene Valeur initiale du numero de scène
   */
  GestionnaireScene(Scene* _scene, unsigned int _numeroScene = 0);
  
  /// La scène actuellement jouée
  std::shared_ptr<Scene> scene;
  
  /// Le numero de scène du gestionnaire.
  /// Il est utilisé en tant qu'identifiant de scène ou comme compteur de scène
  unsigned int numeroScene;
};

#endif /* GESTIONNAIRESCENE_HPP */
