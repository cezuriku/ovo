#ifndef SCENE_HPP
#define SCENE_HPP

#include <SFML/Graphics.hpp>

/**
 * @class Scene
 * @author cedric
 * @date 09/05/15
 * @file Scene.hpp
 * Représente une scène
 */
class Scene {
public:
  /**
   * Initialisation de la scène
   * @param fenetre Fenêtre dans laquelle sera affichée la scène
   */
  virtual void initialiser(sf::RenderWindow& fenetre) = 0;
  
  /**
   * Gère les évènements
   * @param evenement
   * 
   * Gère les évènements qui sont apparu pendant que la scène était affichée
   */
  virtual void gererEvenement(const sf::Event& evenement) = 0;
  
  /**
   * Actualise la scène
   * @param temps Temps en secondes depuis la dernière actualisation
   */
  virtual void actualiser(float temps) = 0;
  
  /**
   * Dessine la scène sur la fenêtre
   * @param fenetre
   */
  virtual void dessiner(sf::RenderWindow& fenetre) = 0;
  
  /**
   * Retourne le statut de la scène
   * 
   * @return 
   * Si le code retour vaut 0 alors la scène n'est pas terminée
   * Si le code retour à une valeur positive alors la scène s'est terminée 
   * normalement et il faut passer à une scène suivante.
   * Si le code retour à une valeur négative alors la scène ne s'est pas
   * terminée correctement et le code doit être considéré comme un code d'erreur
   */
  virtual int getCodeRetour() = 0;
};

#endif // SCENE_HPP
