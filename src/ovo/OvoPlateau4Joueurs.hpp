#ifndef OVOPLATEAU4JOUEURS_HPP
#define OVOPLATEAU4JOUEURS_HPP

#include "OvoPlateau.hpp"

class OvoPlateau4Joueurs : public OvoPlateau {
public:
	OvoPlateau4Joueurs();
	
	bool isJoueurGagnant(int joueur);
		
private:
	void verifierRetournerOeuf(int x, int y);
};

#endif /* OVOPLATEAU4JOUEURS_HPP */ 
