#ifndef OVOPLATEAU_HPP
#define OVOPLATEAU_HPP

#include <list>
#include <tuple>

#include "OvoOeuf.hpp"

class OvoPlateau {
public:
	typedef std::pair<int, int> Position;

	/**
	 * Constructeur par défaut
	 */
	OvoPlateau() : precedenteOrigine(-1, -1) {};
	
	/**
	 * Retourne la liste des positions accessibles depuis l'oeuf
	 * en un seul coup
	 * @param origine La position de l'oeuf
	 * @return La liste des positions accessibles
	 */
	const std::list<Position>& getDeplacementPossible(Position origine);
	
	/**
	 * Affiche le plateau sur la console
	 */
	void afficherConsole() const;
	
	/**
	 * Informe si la case est libre
	 * @param position La position sur le plateau
	 * @return Vrai si la case ne possède pas d'oeuf
	 */
	inline bool isCaseLibre(Position position) const {
		return plateau[position.first][position.second] == nullptr;
	}
	
	/**
	 * Informe si la case est libre
	 * @param x La coordonée en abscisse de la case
	 * @param y La coordonée en ordonnée de la case
	 * @return Vrai si la case ne possède pas d'oeuf
	 */
	inline bool isCaseLibre(int x, int y) const {
		return plateau[x][y] == nullptr;
	}
	
	/**
	 * Informe de la couleur de l'oeuf à cet endroit
	 * @param x La coordonée en abscisse de la case
	 * @param y La coordonée en ordonnée de la case
	 * @return -1 si il n'y a pas d'oeuf et sinon la couleur c'est à dire
	 * 		0 si il est invisible sinon le numero de la couleur (> 0)
	 */
	inline int getVisible(int x, int y) const {
		return plateau[x][y] ? plateau[x][y]->getVisible() : -1;
	}
	
	/**
	 * Déplace un oeuf de l'origine vers la destination
	 * si l'oeuf existe et que le coup est possible
	 * @param ox La coordonée en abscisse de l'oeuf au départ
	 * @param oy La coordonée en ordonnée de l'oeuf au départ
	 * @param dx La coordonée en abscisse de l'oeuf à l'arrivée
	 * @param dy La coordonée en ordonnée de l'oeuf à l'arrivée
	 * @return Vrai si l'oeuf à été déplacé
	 */
	bool deplacer(int ox, int oy, int dx, int dy);
	
	/**
	 * Donne la couleur du dessous de l'oeuf
	 * @param x La coordonée en abscisse de l'oeuf
	 * @param y La coordonée en ordonnée de l'oeuf
	 * @return 0 si l'oeuf n'est pas retourné sinon sa couleur
	 * 	lorsqu'il est retourné
	 */
	int getCouleurDessous(int x, int y) const {
		if (plateau[x][y]->getVisible() == 0)
			return plateau[x][y]->getCouleur();
		
		return 0;
	}
	
	/**
	 * Informe si le joueur est gagnant
	 * et vire tous ses oeufs s'il ne l'est pas
	 */
	virtual bool isJoueurGagnant(int joueur) = 0;
	
	/**
	 * Supprime tous les oeufs du joueur
	 * @param joueur le numero du joueur à supprimer
	 */
	void supprimerJoueur(int joueur);
	
protected:
	OvoOeuf::uPtr plateau[6][6];

private:
	/**
	 * Vérifie et retourne l'oeuf si il doit être retourné
	 * @param x La coordonée en abscisse de l'oeuf
	 * @param y La coordonée en ordonnée de l'oeuf
	 */
	virtual void verifierRetournerOeuf(int x, int y) = 0;
	
	Position precedenteOrigine;
	std::list<Position> precedentDeplacement;
};

#endif /* OVOPLATEAU_HPP */
 
