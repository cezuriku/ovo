#ifndef OVOOEUF_HPP
#define OVOOEUF_HPP

#include <exception>
#include <memory>

class OvoOeuf {
public:
	typedef std::unique_ptr<OvoOeuf> uPtr;

	/**
	 * Constructeur par défaut
	 * Il initialise l'oeuf avec le numero de sa couleur (> 0)
	 * et indique qu'il est non retourné (donc visible)
	 * @param _couleur La couleur de l'oeuf
	 */
	inline OvoOeuf(int _couleur) :
		couleur(_couleur), retourne(false) {
			if (couleur == 0)
				throw std::logic_error(
					"L'oeuf ne peut pas avoir la couleur 0");
	}
	
	/**
	 * Récupère la couleur visible de l'oeuf
	 * si il est retourné alors retourne 0
	 * @return La couleur visible de l'oeuf
	 */
	inline int getVisible() {
		return retourne ? 0 : couleur;
	}
	
	/**
	 * Récupère la couleur de l'oeuf
	 * @return La couleur de l'oeuf
	 */
	inline int getCouleur() {
		return couleur;
	}
	
	/**
	 * Informe si l'oeuf est retourné
	 * @return L'oeuf est retourné ?
	 */
	inline bool isRetourne() {
		return retourne;
	}
	 
	
	/**
	 * Change le statut retourné de l'oeuf
	 * @param _retourne Le nouveau statut retourné de l'oeuf
	 */
	inline void setRetourne(bool _retourne = true) {
		retourne = _retourne;
	}
	
	
private:
	int couleur;
	bool retourne;
};

#endif /* OVOOEUF_HPP */ 
