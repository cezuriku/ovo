#ifndef OVOPLATEAU2JOUEURS_HPP
#define OVOPLATEAU2JOUEURS_HPP

#include "OvoPlateau.hpp"

class OvoPlateau2Joueurs : public OvoPlateau {
public:
	OvoPlateau2Joueurs();
	
	bool isJoueurGagnant(int joueur);
		
private:
	void verifierRetournerOeuf(int x, int y);
};

#endif /* OVOPLATEAU2JOUEURS_HPP */ 
