#define COULEUR_ORANGE sf::Color(254, 153, 1)
#define COULEUR_BLEU sf::Color(51, 255, 254)
#define COULEUR_ORANGE_TRANSPARENT sf::Color(254, 153, 1, 150)
#define COULEUR_BLEU_TRANSPARENT sf::Color(51, 255, 254, 150)

#include <iostream>
#include "SceneJeuOvo.hpp"

void SceneJeuOvo::initialiser(sf::RenderWindow& _fenetre) {
	if (!policeTexteFin.loadFromFile("ressources/MorrisRomanBlack.otf")) {
		std::cerr << "Erreur lors du chargement de la police \"MorrisRomanBlack.otf\"" << std::endl;
	}
	
	fenetre = &_fenetre;
	
	texteFin.setFont(policeTexteFin);
	texteFin.setString("J'ai fini!");
	texteFin.setCharacterSize(48);
	
	sf::FloatRect texteDimensions = texteFin.getGlobalBounds();
	
	boutonFin.setSize(sf::Vector2f(
		texteDimensions.width * 1.5, texteDimensions.height * 1.5));
	boutonFin.setFillColor(COULEUR_BLEU);
	
	texteFin.setOrigin(sf::Vector2f(
		texteDimensions.width / 2,
		14 + texteDimensions.height / 2));
	
	fond.setFillColor(COULEUR_ORANGE);
	
	trouOeuf.setFillColor(sf::Color::White);
	oeuf.setFillColor(sf::Color::Black);
	oeufPossible.setFillColor(COULEUR_ORANGE_TRANSPARENT);
	
	symboleOrange.setPointCount(3);
	symboleOrange.setFillColor(COULEUR_ORANGE);
	
	symboleBleu.setPointCount(6);
	symboleBleu.setFillColor(COULEUR_BLEU);
	
	posPrecedente.first = -1;
	
	redimensionner(fenetre->getSize().x, fenetre->getSize().y);
}

void SceneJeuOvo::redimensionner(unsigned int fenX, unsigned int fenY) {
	
	if (fenX * 5 / 7 > fenY * 5 /6)
		tailleFond = fenY * 4 / 6;
	else
		tailleFond = fenX * 5 / 7;
		
	origineFond.x = (fenX - tailleFond) / 2; 
	origineFond.y = (fenY * 5 / 6 - tailleFond) / 2;
	
	fond.setSize(sf::Vector2f(tailleFond, tailleFond));
	fond.setPosition(origineFond);
	
	trouOeuf.setRadius(tailleFond / 14);
	trouOeuf.setOrigin(sf::Vector2f(tailleFond / 14, tailleFond / 14));
	oeuf.setRadius(tailleFond / 14);
	oeuf.setOrigin(sf::Vector2f(tailleFond / 14, tailleFond / 14));
	oeufPossible.setRadius(tailleFond / 14);
	oeufPossible.setOrigin(sf::Vector2f(tailleFond / 14, tailleFond / 14));
	symboleOrange.setRadius(tailleFond / 45);
	symboleOrange.setOrigin(sf::Vector2f(tailleFond / 45, tailleFond / 45));
	symboleBleu.setRadius(tailleFond / 45);
	symboleBleu.setOrigin(sf::Vector2f(tailleFond / 45, tailleFond / 45));
	
	texteFin.setPosition(sf::Vector2f(fenX / 2, fenY * 25 / 28));
	
	sf::FloatRect texteDimensions = texteFin.getGlobalBounds();
	
	boutonFin.setPosition(sf::Vector2f(
		texteDimensions.left - texteDimensions.width * 0.25,
		texteDimensions.top - texteDimensions.height * 0.25));
}
  
void SceneJeuOvo::gererEvenement(const sf::Event& evenement) {
	switch (evenement.type) {
		case sf::Event::Resized: {
			redimensionner(evenement.size.width , evenement.size.height);
			sf::View vue;
			vue.setSize(sf::Vector2f(
				fenetre->getSize().x, fenetre->getSize().y));
			vue.setCenter(sf::Vector2f(
				fenetre->getSize().x / 2, fenetre->getSize().y / 2));
			fenetre->setView(vue);
		}
			break;
			
		case sf::Event::KeyPressed:
			if (evenement.key.code == sf::Keyboard::Escape)
				retour = 1;
			break;
			
		case sf::Event::MouseButtonPressed: {
			int x = static_cast<int>(evenement.mouseButton.x - origineFond.x);
			int y = static_cast<int>(evenement.mouseButton.y - origineFond.y);
			
			OvoPlateau::Position posActuelle(
				x / (static_cast<int>(tailleFond) / 6),
				y / (static_cast<int>(tailleFond) / 6));
			
			if (x >= 0 && x < static_cast<int>(tailleFond) &&
				y >= 0 && y < static_cast<int>(tailleFond)) {
					
					int visible = plateau.getVisible(posActuelle.first, posActuelle.second);
					
					// A t'on cliqué sur un oeuf?
					if (visible != -1) {
						
						// Est-ce l'oeuf de l'adversaire?
						if (visible != 0 && visible != joueurActuel)
							posPrecedente.first = -1;
						else {
							if (posPrecedente == posActuelle) {
								//Retourner Oeuf
								if (visible == 0)
									afficherCouleurOeuf = true;
	
							} else {
								posPrecedente = posActuelle;
							}
						}
					
					} else if (posPrecedente.first != -1 &&
						plateau.deplacer(posPrecedente.first, posPrecedente.second,
						posActuelle.first, posActuelle.second)) {
						changerJoueur();
					} else
						posPrecedente.first = -1;
				} else {
					if (boutonFin.getGlobalBounds().contains(
						evenement.mouseButton.x, evenement.mouseButton.y)) {
						if (plateau.isJoueurGagnant((joueurActuel % 2) + 1)) {
							std::cout << "Tu as gagné :)" << std::endl;
							retour = joueurActuel * 2 + 1;
						} else {
							retour = joueurActuel * 2;
							std::cout << "Tu as perdu :)" << std::endl;
						}
					}	
					posPrecedente.first = -1;
				}
			}
			break;
			
		case sf::Event::MouseButtonReleased:
			if (afficherCouleurOeuf == true) {
				afficherCouleurOeuf = false;
				changerJoueur();
			}
			break;
		
		default:
			break;
	}
}

void SceneJeuOvo::actualiser(float /* temps */) {
	
}

void SceneJeuOvo::dessiner(sf::RenderWindow& /* fenetre */) {
	fenetre->clear(sf::Color::Black);
	
	fenetre->draw(fond);

	int couleur;
	
	for (unsigned int i = 0; i < 6; i++) {	
		for (unsigned int j = 0; j < 6; j++) {
			couleur = plateau.getVisible(static_cast<int>(i), static_cast<int>(j));
			
			if (couleur == -1) {
				
				trouOeuf.setPosition(
					origineFond.x + static_cast<int>(tailleFond) / 6 * static_cast<int>(i)
						+ static_cast<int>(tailleFond / 12),
					origineFond.y + static_cast<int>(tailleFond) / 6 * static_cast<int>(j)
						+ static_cast<int>(tailleFond / 12));
					
				fenetre->draw(trouOeuf);
			} else {
				if (afficherCouleurOeuf == true &&
					static_cast<int>(i) == posPrecedente.first &&
					static_cast<int>(j) == posPrecedente.second)
					couleur = plateau.getCouleurDessous(static_cast<int>(i), static_cast<int>(j));
				
				oeuf.setPosition(
					origineFond.x + static_cast<int>(tailleFond) / 6 * static_cast<int>(i)
						+ static_cast<int>(tailleFond / 12),
					origineFond.y + static_cast<int>(tailleFond) / 6 * static_cast<int>(j)
						+ static_cast<int>(tailleFond / 12));
			
				
				fenetre->draw(oeuf);
				
				if (couleur == 1) {
					symboleOrange.setPosition(
						origineFond.x + static_cast<int>(tailleFond / 6 * i) 
							+ static_cast<int>(tailleFond / 12),
						origineFond.y + static_cast<int>(tailleFond / 6 * j) 
							+ static_cast<int>(tailleFond / 12));
							
					fenetre->draw(symboleOrange);
				} else if (couleur == 2) {
					
					symboleBleu.setPosition(
						origineFond.x + static_cast<int>(tailleFond / 6 * i) 
							+ static_cast<int>(tailleFond / 12),
						origineFond.y + static_cast<int>(tailleFond / 6 * j) 
							+ static_cast<int>(tailleFond / 12));
							
					fenetre->draw(symboleBleu);
				} 
				
			}
			
		}
	}
	
	if (posPrecedente.first != -1) {
		const std::list<OvoPlateau::Position>& liste(
			plateau.getDeplacementPossible(posPrecedente));
		
		for (std::list<OvoPlateau::Position>::const_iterator it = liste.begin();
			it != liste.end(); ++it) {
			
			oeufPossible.setPosition(
				origineFond.x + static_cast<int>(tailleFond) / 6 * it->first 
					+ static_cast<int>(tailleFond / 12),
				origineFond.y + static_cast<int>(tailleFond) / 6 * it->second 
					+ static_cast<int>(tailleFond / 12));
			
			fenetre->draw(oeufPossible);
		}
	}
	
	fenetre->draw(boutonFin);
	fenetre->draw(texteFin);
}

void SceneJeuOvo::changerJoueur() {
	joueurActuel = joueurActuel % 2 + 1;
	if (joueurActuel == 1) {
		fond.setFillColor(COULEUR_ORANGE);
		boutonFin.setFillColor(COULEUR_BLEU);
		oeufPossible.setFillColor(COULEUR_ORANGE_TRANSPARENT);
	} else {
		fond.setFillColor(COULEUR_BLEU);
		boutonFin.setFillColor(COULEUR_ORANGE);
		oeufPossible.setFillColor(COULEUR_BLEU_TRANSPARENT);
	}
	posPrecedente.first = -1;
}
