#include <algorithm>
#include <iostream>

#include "OvoPlateau.hpp"

const std::list<OvoPlateau::Position>& OvoPlateau::getDeplacementPossible(
	Position origine) {
	std::list<Position> liste;
	
	if (precedenteOrigine.first == origine.first &&
		precedenteOrigine.second == origine.second)
		return precedentDeplacement;
	
	precedenteOrigine = origine;
	precedentDeplacement.clear();
	
	if (isCaseLibre(origine))
		return precedentDeplacement;
	
	Position p2;

	for (int dx = -1; dx <= 1; ++dx) {
		for (int dy = -1; dy <= 1; ++dy) {
			p2 = std::make_pair(
				origine.first + dx,
				origine.second + dy);
			
			if (p2.first >= 0 && p2.first <= 5 &&
				p2.second >= 0 && p2.second <= 5) {
				if (isCaseLibre(p2))
					precedentDeplacement.push_back(p2);
				else {
					p2 = std::make_pair(
						p2.first + dx,
						p2.second + dy);
					if (p2.first >= 0 && p2.first <= 5 &&
						p2.second >= 0 && p2.second <= 5)
						if (isCaseLibre(p2))
							liste.push_back(p2);
				}
			}
		}
	}
	
	std::list<Position>::iterator p;
	bool existe;
	while (!liste.empty()) {
		p = liste.begin();
		for (int dx = -1; dx <= 1; ++dx) {
			for (int dy = -1; dy <= 1; ++dy) {
				p2 = std::make_pair(
					std::get<0>(*p) + dx,
					std::get<1>(*p) + dy);
				
				if (p2.first >= 0 && p2.first <= 5 &&
					p2.second >= 0 && p2.second <= 5 &&
					!isCaseLibre(p2)) {
					
					p2 = std::make_pair(
						p2.first + dx,
						p2.second + dy);

					if (p2.first >= 0 && p2.first <= 5 &&
						p2.second >= 0 && p2.second <= 5 &&
						isCaseLibre(p2)) {
						
						existe = false;
						for (Position p3 : liste)
							if (p3.first == p2.first &&
								p3.second == p2.second)
								existe = true;

						if (!existe) {
							for (Position p3 : precedentDeplacement)
								if (p3.first == p2.first &&
									p3.second == p2.second)
									existe = true;
							
							if (!existe)
								liste.push_back(p2);
						}
					}
				}
			}
		}
		precedentDeplacement.push_back(*p);
		liste.pop_front();
	}
		
	
	return precedentDeplacement;
}

void OvoPlateau::afficherConsole() const {
	for (int y = 0; y < 6; y++) {
		for (int x = 0; x < 6; x++) {
			if (plateau[x][y])
				std::cout << plateau[x][y]->getVisible() << " ";
			else
				std::cout << "  ";
		}
		std::cout << "\n";
	 }
	 std::cout << std::endl;
}

bool OvoPlateau::deplacer(int ox, int oy, int dx, int dy) {
	if (precedenteOrigine.first != ox ||
		precedenteOrigine.second != oy) {
		getDeplacementPossible(std::make_pair(ox, oy));
	}
	
	bool existe = false;
	for (Position p : precedentDeplacement)
		if (dx == p.first &&
			dy == p.second)
			existe = true;
	
	if (existe) {
		plateau[dx][dy]
			.reset(plateau[ox][oy].release());
		precedenteOrigine = std::make_pair(-1,-1);
		verifierRetournerOeuf(dx, dy);
	}
	return existe;
}

void OvoPlateau::supprimerJoueur(int joueur) {
	for (int x = 0; x < 6; ++x)
		for (int y = 0; y < 6; ++y)
			if (plateau[x][y] &&
				plateau[x][y]->getCouleur() == joueur)
				plateau[x][y].reset();
}
