#ifndef SCENEJEUOVO_HPP
#define SCENEJEUOVO_HPP

#include <SFML/Graphics.hpp>

#include "../Scene.hpp"
#include "OvoPlateau2Joueurs.hpp"

class SceneJeuOvo: public Scene {
public:
	SceneJeuOvo() : retour(0), joueurActuel(1),
		afficherCouleurOeuf(false) { }
	
	void initialiser(sf::RenderWindow& _fenetre);
  
	void gererEvenement(const sf::Event& evenement);

	void actualiser(float temps);

	void dessiner(sf::RenderWindow& fenetre);

	inline int getCodeRetour() {
		return retour;
	}
		
private:
	void changerJoueur();
	
	void redimensionner(unsigned int fenX, unsigned int fenY);
	
	
	// Attributs
	
	sf::RenderWindow* fenetre;

	int retour;
	
	OvoPlateau2Joueurs plateau;
	
	int joueurActuel;
	
	sf::RectangleShape fond;
	unsigned int tailleFond;
	sf::Vector2f origineFond;
	
	sf::CircleShape trouOeuf;
	sf::CircleShape oeuf;
	sf::CircleShape oeufPossible;
	sf::CircleShape symboleOrange;
	sf::CircleShape symboleBleu;
	
	sf::Font policeTexteFin;
	sf::Text texteFin;
	
	sf::RectangleShape boutonFin;
	sf::Vector2f tailleBoutonFin;
	sf::Vector2f origineBoutonFin;
	
	/// position sur le plateau du précédent click
	std::pair<int, int> posPrecedente;
	
	bool afficherCouleurOeuf;

};

#endif /* SCENEJEUOVO_HPP */ 
