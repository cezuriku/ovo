#include "OvoPlateau4Joueurs.hpp"

OvoPlateau4Joueurs::OvoPlateau4Joueurs() {
	for (int i = 0; i < 4; ++i) {
		plateau[i%2][i/2].reset(new OvoOeuf(1));
		plateau[4 + i%2][i/2].reset(new OvoOeuf(2));
		plateau[i%2][4 + i/2].reset(new OvoOeuf(3));
		plateau[4 + i%2][4 + i/2].reset(new OvoOeuf(4));
	}
}

void OvoPlateau4Joueurs::verifierRetournerOeuf(int x, int y) {
	if (plateau[x][y]->getVisible() != 0) {
		if (x <= 1 && y <= 1 && plateau[x][y]->getVisible() != 1)
			plateau[x][y]->setRetourne();
			
		if (x >= 4 && y <= 1 && plateau[x][y]->getVisible() != 2)
			plateau[x][y]->setRetourne();
			
		if (x <= 1 && y >= 4 && plateau[x][y]->getVisible() != 3)
			plateau[x][y]->setRetourne();
			
		if (x >=4 && y >= 4 && plateau[x][y]->getVisible() != 4)
			plateau[x][y]->setRetourne();
	}
}

bool OvoPlateau4Joueurs::isJoueurGagnant(int joueur) {
	bool gagnant = true;
	
	switch (joueur) {
		case 1:
			for (int i = 0; i < 4; ++i) {
				if (!plateau[i%2][i/2])
					gagnant = false;
				else if (plateau[i%2][i/2]->getVisible() != 0 ||
					plateau[i%2][i/2]->getCouleur() != 1)
					gagnant = false;
			}
			break;
			
		case 2:
			for (int i = 0; i < 4; ++i) {
				if (!plateau[4 + i%2][i/2])
					gagnant = false;
				else if (plateau[4 + i%2][i/2]->getVisible() != 0 ||
					plateau[4 + i%2][i/2]->getCouleur() != 2)
					gagnant = false;
			}
			break;
			
		case 3:
			for (int i = 0; i < 4; ++i) {
				if (!plateau[i%2][4 + i/2])
					gagnant = false;
				else if (plateau[i%2][4 + i/2]->getVisible() != 0 ||
					plateau[i%2][4 + i/2]->getCouleur() != 3)
					gagnant = false;
			}
			break;
			
		case 4:
			for (int i = 0; i < 4; ++i) {
				if (!plateau[4 + i%2][4 + i/2])
					gagnant = false;
				else if (plateau[4 + i%2][4 + i/2]->getVisible() != 0 ||
					plateau[4 + i%2][4 + i/2]->getCouleur() != 4)
					gagnant = false;
			}
			break;
			
		default:
			gagnant = false;
			break;
	}
	
	if (joueur >= 1 && joueur <= 4 && !gagnant)
		supprimerJoueur(joueur);
	
	return gagnant;
}
