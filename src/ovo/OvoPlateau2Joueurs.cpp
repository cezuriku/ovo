#include "OvoPlateau2Joueurs.hpp"

OvoPlateau2Joueurs::OvoPlateau2Joueurs() {
	for (int i = 0; i < 6; ++i) {
		plateau[0][i].reset(new OvoOeuf(1));
		plateau[5][i].reset(new OvoOeuf(2));
	}
}

void OvoPlateau2Joueurs::verifierRetournerOeuf(int x, int y) {
	if (plateau[x][y]->getVisible() == 1) {
		if (x == 5)
			plateau[x][y]->setRetourne();
	} else if (plateau[x][y]->getVisible() == 2) {
		if (x == 0)
			plateau[x][y]->setRetourne();
	}
}

bool OvoPlateau2Joueurs::isJoueurGagnant(int joueur) {
	bool gagnant = false;
	
	if (joueur == 1) {
		
		gagnant = true;
		for (int i = 0; i < 6; ++i) {
			if (!plateau[0][i] || plateau[0][i]->getVisible() != 0 ||
				plateau[0][i]->getCouleur() != 1)
				gagnant = false;
		}
	} else if (joueur == 2) {
		
		gagnant = true;
		for (int i = 0; i < 6; ++i) {
			if (!plateau[5][i] || plateau[5][i]->getVisible() != 0 ||
				plateau[5][i]->getCouleur() != 2)
				gagnant = false;
		}
	}
	
	return gagnant;
}
