#include <iostream>
#include <memory>
#include <SFML/Graphics.hpp>
#include "GestionnaireOvo.hpp"
#include "Scene.hpp"

int main(void) {
  sf::ContextSettings config;
	config.antialiasingLevel = 1;
  
  sf::RenderWindow fenetre(sf::VideoMode(800, 600), "Ovo",
		sf::Style::Default, config);
  fenetre.setFramerateLimit(30);
  
  std::unique_ptr<GestionnaireScene> gestionnaire(new GestionnaireOvo());
  std::shared_ptr<Scene> scene(gestionnaire->getScene());
  scene->initialiser(fenetre);
  
	sf::Clock temps;
  
  while (fenetre.isOpen()) {
    
    sf::Event evenement;
    while (fenetre.pollEvent(evenement)) {
      if (evenement.type == sf::Event::Closed)
        fenetre.close();
      else
        scene->gererEvenement(evenement);
    }
    
    scene->actualiser(temps.restart().asSeconds());
    scene->dessiner(fenetre);
    fenetre.display();
    
    if (gestionnaire->changementDeScene()) {
      scene = gestionnaire->getScene();
      if (scene)
        scene->initialiser(fenetre);
      else
        fenetre.close();
    }
  }
  
  return EXIT_SUCCESS;
}
